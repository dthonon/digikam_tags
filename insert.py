# -*- coding: utf-8 -*-

import networkx as nx
import re
import random 
import mysql.connector as mdb
 
class TagNumber:
    def __init__(self):
        self.counter = 400

    def __next__(self):
        self.counter += 1
        return self.counter


def build_graph(tag_file):
    graph = nx.DiGraph()
    depth = 0
    node_name = ''
    with open(tag_file, encoding='utf-8') as f:
        for line in f:
            prev_depth = depth
            prev_node = node_name
            tagging = re.search('^(\s*)(.*)', line)
            depth = len(tagging.group(1))
            node_name = tagging.group(2)
            if graph.has_node(node_name):
                new_name = node_name +  '@' + str(random.randint(1000000,9000000) )
                #print('>>Renaming duplicate node {} to {}'.format(node_name, new_name))
                node_name = new_name
            # print('d={}, t={}'.format(depth, node_name))
            if line.find("{") == -1:
                # Not yet handlind synonyms enclosed in {}
                if depth == 0:
                    # Top of a new tree
                    graph.add_edge('root', node_name)
                elif depth > prev_depth:
                    # Moving down the tree: add node under previous node
                    graph.add_edge(prev_node, node_name)
                else:
                    # Moving at same or upper level of the tree:
                    #  a) climbing until reaching 1 level up
                    #  b) adding node under that level
                    for l in range(0, prev_depth-depth+1):
                        # print('Moving up from {}'.format(prev_node))
                        prev_node = list(graph.predecessors(prev_node))[0]
                        # print('To {}'.format(prev_node))
                    graph.add_edge(prev_node, node_name)
            else:
                # Synonym: modify upper node name
                new_name = prev_node + ' ' + node_name
                # print('Synonym: {} => {}'.format(prev_node, new_name))
                graph = nx.relabel_nodes(graph, {prev_node: new_name}, copy=False)
                node_name = new_name
                depth -= 1
                
    
    return graph


def print_graph(graph):
    for s, t in nx.dfs_edges(graph, source='root'):
        print('  '*(nx.shortest_path_length(graph, source='root', target=t)-1), t)

    # nx.draw_shell(graph, with_labels=True)


def number_graph(graph, parent, pid, depth, number):
    for n in graph.successors(parent):
        id = next(number)
        graph.nodes[n]['id'] = id
        graph.nodes[n]['pid'] = pid
#        print('{}id={}, pid={}, node={}'.format('  '*depth,
#                                                graph.nodes[n]['id'], 
#                                                graph.nodes[n]['pid'],
#                                                n))
        number_graph(graph, n, id, depth+1, number)
    return graph


def write_graph(graph, parent, depth, cursor):
    for n in graph.successors(parent):
        syn = n.find('@')
        if syn == -1:
            tag_name = n
        else:
            tag_name = n[0:syn]
            
        sql = "INSERT INTO tags (id, pid, name, lft, rgt) VALUES(%s, %s, %s, %s, %s)"
        try:
            cursor.execute(sql, (graph.nodes[n]['id'], graph.nodes[n]['pid'], tag_name, 1, 2))
            # print(cursor.statement)
        except:
            print('Exception raised: id={}, pid={}, node={}'.format(graph.nodes[n]['id'], 
                                                graph.nodes[n]['pid'],
                                                n))
            raise
            
        write_graph(graph, n, depth+1, cursor)
    return graph


class ParamsDebug:
    def __init__(self):
        self.host = 'localhost'
        self.user = 'digikam'
        self.password = 'TRybsEkFiShrac2'
        self.database = 'digikam'

def get_connection_and_cursor(params):
    con = mdb.connect(host=params.host,
                      user=params.user,
                      password=params.password,
                      database=params.database)
    cursor = con.cursor()
    return con, cursor

    
def main():
    
    params = ParamsDebug()
    number = TagNumber()
    
    print('Building graph')
    graph = build_graph('TagsLR.txt')
    graph.nodes['root']['id'] = 0
    print('Numbering graph')
    graph = number_graph(graph, 'root', 0, 0, number)
    # print_graph(graph)

    print('Writing graph')
    connection, cursor = get_connection_and_cursor(params)
    write_graph(graph, 'root', 0, cursor)
    connection.commit()
    connection.close()

if __name__ == '__main__':
    main()
    